package WebsiteURL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class BasicChecks
{

public static void main(String args[]) throws InterruptedIOException
{
try
{
    
//To Read Excel File
File src = new File("E:\\CitiBank\\SGGCB_v4.xlsx");
FileInputStream fis = new FileInputStream(src);
XSSFWorkbook workbook = new XSSFWorkbook(fis);
XSSFSheet sheet = workbook.getSheetAt(0);

//To Open Chrome Driver
System.setProperty("webdriver.chrome.driver", "D:\\download\\chromedriver.exe");
WebDriver driver = new ChromeDriver();

for(int i=3000; i<=3010; i++)
{
String url= sheet.getRow(i).getCell(0).getStringCellValue();

driver.get(url);

// To Handle unexpected Alert
try
{
    driver.switchTo().alert().accept();
    Thread.sleep(4000);

    // To Check Either the Page is Form Page or Not
String pagesrc = driver.getPageSource();
if(pagesrc.contains("<Form") || pagesrc.contains("< Form") || pagesrc.contains("<form") || pagesrc.contains("< form"))

{
    System.out.println(i + url + "Form Page");
Cell cell = sheet.getRow(i).createCell(1);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Form Page");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
workbook.write(fos);
}

else

{
    System.out.println(i + url + "Not a form Page");
Cell cell = sheet.getRow(i).createCell(1);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Not a form Page");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
workbook.write(fos);

}

// To Check Either the Page is Error (404) Page or Not
if(pagesrc.contains("errorPage404") || pagesrc.contains("The page you are looking for") )

{
    System.out.println(i + url + "Error Page 404");
Cell cell = sheet.getRow(i).createCell(2);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Error Page 404");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
workbook.write(fos);
}

else
    
{
    System.out.println(i + url + "Landing Page");
Cell cell = sheet.getRow(i).createCell(2);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Landing Page");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
workbook.write(fos);

}

//To Check the page has SiteCatalyst code or Not
if(pagesrc.contains("code.js") || pagesrc.contains("lyst.js") || pagesrc.contains("lyst_ru.js") || pagesrc.contains("code1.js"))

{
    System.out.println(i + url + "SC code Tagged");
Cell cell = sheet.getRow(i).createCell(3);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("SC code Tagged");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);
}

else

{
    System.out.println(i + url + "SC code not Tagged");
Cell cell = sheet.getRow(i).createCell(3);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("SC code Not Tagged ");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);

}

//To Check the page has Ensighten code or Not
if(pagesrc.contains("ensighten.js") || pagesrc.contains("strap.js") || pagesrc.contains("nexus"))

{
    System.out.println(i + url + "Ensighten Tagged");
Cell cell = sheet.getRow(i).createCell(4);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Ensighten Tagged");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);
}

else
    
{
    System.out.println(i + url + "Ensighten Not Tagged");
Cell cell = sheet.getRow(i).createCell(4);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Ensighten Not Tagged");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);
}

}
catch(Exception e)
{
    Thread.sleep(4000);
    String pagesrc = driver.getPageSource();
    
    // To Handle Exceptions for Form Page / Not
   if(pagesrc.contains("<Form") || pagesrc.contains("< Form") || pagesrc.contains("<form") || pagesrc.contains("< form"))


    {
        System.out.println(i + url + "Form Page");
    Cell cell = sheet.getRow(i).createCell(1);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("Form Page");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
    workbook.write(fos);
    }
    
    else
        
    {
        System.out.println(i + url + "Not a form Page");
    Cell cell = sheet.getRow(i).createCell(1);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("Not a form Page");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
    workbook.write(fos);

    }
    
    // To Handle Exceptions For Error Page(404) / Not
    if(pagesrc.contains("errorPage404") || pagesrc.contains("The page you are looking for") )

    {
        System.out.println(i + url + "Error Page 404");
    Cell cell = sheet.getRow(i).createCell(2);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("Error Page 404");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
    workbook.write(fos);
    }
    
    else
        
    {
        System.out.println(i + url + "Landing Page");
    Cell cell = sheet.getRow(i).createCell(2);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("Landing Page");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_v4.xlsx");
    workbook.write(fos);

}
    
  //To Handle Exceptions SiteCatalyst code Present or Not
    if(pagesrc.contains("code.js") || pagesrc.contains("lyst.js") || pagesrc.contains("lyst_ru.js") || pagesrc.contains("code1.js"))

    {
        System.out.println(i + url + "SC code Tagged");
    Cell cell = sheet.getRow(i).createCell(3);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("SC code Tagged");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
    workbook.write(fos);
    }

    else

    {
        System.out.println(i + url + "SC code not Tagged");
    Cell cell = sheet.getRow(i).createCell(3);
    cell.setCellType(cell.CELL_TYPE_STRING);
    cell.setCellValue("SC code Not Tagged ");
    FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
    workbook.write(fos);

    }


//To Handle Exceptions Ensighteen code Present or Not
if(pagesrc.contains("ensighten.js") || pagesrc.contains("strap.js") || pagesrc.contains("nexus"))

{
    System.out.println(i + url + "Ensighten Tagged");
Cell cell = sheet.getRow(i).createCell(4);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Ensighten Tagged");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);
}

else
    
{
    System.out.println(i + url + "Ensighten Not Tagged");
Cell cell = sheet.getRow(i).createCell(4);
cell.setCellType(cell.CELL_TYPE_STRING);
cell.setCellValue("Ensighten Not Tagged");
FileOutputStream fos = new FileOutputStream("E:\\CitiBank\\SGGCB_final.xlsx");
workbook.write(fos);
}
}
}
}

catch (Exception e)
{
System.out.println(e.getMessage());
}
}

}